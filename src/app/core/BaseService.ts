/**
 *
 * Base service for SW API resources
 * Films, People, Planets, Species, StarShips and Vehicles
 *
 */
import { HttpClient, HttpParams } from '@angular/common/http';
import { SWAPI_URL } from './config';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PlotDataService } from '../services/plot-data.service';


export class BaseService {

  private dataSource = new BehaviorSubject<IList>(null);

  public $list = this.dataSource.asObservable();

  private icons = [];
  private routeBase: string;


  constructor(
    public http: HttpClient,
    public plotData: PlotDataService,
    routeBase: string,
  ) {
    this.routeBase = routeBase;
    this.icons['IFilms'] = 'play';
    this.icons['IPeople'] = 'user';
    this.icons['IPlanets'] = 'globe';
    this.icons['ISpecies'] = 'users';
    this.icons['IStarShips'] = 'space-shuttle';
    this.icons['IVehicles'] = 'car';
  }


  /**
   * retrive data for list resource,
   * ie. https://swapi.co/api/films
   */
  getAll(section: string, url?: string) {

    url = url || SWAPI_URL + this.routeBase;

    return this.http.get<IList>(url).pipe(tap((res) => {

      for (let i = 0; i < res.results.length; i++) {
        res.results[i].name = res.results[i].name || res.results[i].title;
        res.results[i]._section = section;
        res.results[i]._checked = this.plotData.exists(res.results[i]);
        res.results[i]._icon = this.icons[section];
      }

      this.dataSource.next(res);
    }));
  }


  /**
   * Retrive resource by ID,
   * ie. https://swapi.co/api/films/1
   */
  getByID(section: string, id: number): Observable<any> {
    return this.http.get<IList>(SWAPI_URL + this.routeBase + id).pipe(tap((res) => {
      res.name = res.name || res.title;
      res._section = section;
      res._checked = true;
      res._icon = this.icons[section];

      this.checkItem(res);
    }));
  }


  /**
   * Get number of total results in one section,
   * ie. Films returns 7
   */
  getCount(): number {
    return this.dataSource.value.count;
  }


  /**
   * Performs search over one section
   * https://swapi.co/api/films/?search=something
   */
  search(section: string, params: HttpParams): Observable<any> {
    return this.http.get<IList>(SWAPI_URL + this.routeBase, {params}).pipe(tap((res) => {

      for (let i = 0; i < res.results.length; i++) {
        res.results[i].name = res.results[i].name || res.results[i].title;
        res.results[i]._section = section;
        res.results[i]._checked = this.plotData.exists(res.results[i]);
        res.results[i]._icon = this.icons[section];
      }

      this.dataSource.next(res);
    }));
  }


  /**
   * Clear all selected items from list
   */
  clearAll() {
    const res = this.dataSource.value;
    for (let i = 0; i < res.results.length; i++) {
      res.results[i]._checked = false;
    }
    this.dataSource.next(res);
  }


  /**
   * Select one item on list
   */
  checkItem(item) {
    const res = this.dataSource.value;
    for (let i = 0; i < res.results.length; i++) {
      if (!(res.results[i]._section === item._section && res.results[i].name === item.name)) {
        continue;
      }
      res.results[i]._checked = true;
    }
    this.dataSource.next(res);
  }


  /**
   * Clear selection from one item on list
   */
  uncheckItem(item) {
    const res = this.dataSource.value;
    for (let i = 0; i < res.results.length; i++) {
      if (!(res.results[i]._section === item._section && res.results[i].name === item.name)) {
        continue;
      }
      res.results[i]._checked = false;
    }
    this.dataSource.next(res);
  }
}

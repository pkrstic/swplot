import { FilmsService } from '../services/films.service';
import { PeopleService } from '../services/people.service';
import { PlanetsService } from '../services/planets.service';
import { StarshipsService } from '../services/starships.service';
import { VehiclesService } from '../services/vehicles.service';
import { SpeciesService } from '../services/species.service';


export const SWAPI_URL = 'https://swapi.co/api';

export type CommonService = FilmsService | PeopleService | PlanetsService | SpeciesService | StarshipsService | VehiclesService;

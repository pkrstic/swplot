import { TestBed } from '@angular/core/testing';

import { ICanHazDadJokesService } from './ican-haz-dad-jokes.service';

describe('ICanHazDadJokesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ICanHazDadJokesService = TestBed.get(ICanHazDadJokesService);
    expect(service).toBeTruthy();
  });
});

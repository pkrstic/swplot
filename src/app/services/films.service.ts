import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../core/BaseService';
import { PlotDataService } from './plot-data.service';


@Injectable({
  providedIn: 'root',
})
export class FilmsService extends BaseService {

  constructor(http: HttpClient, plotData: PlotDataService) {
    super(http, plotData, '/films/');
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../core/BaseService';
import { PlotDataService } from './plot-data.service';


@Injectable({
  providedIn: 'root',
})
export class StarshipsService extends BaseService {

  constructor(http: HttpClient, plotData: PlotDataService) {
    super(http, plotData, '/starships/');
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class PlotDataService {

  private dataSource = new BehaviorSubject<ICommon[]>([]);
  private titleSource = new BehaviorSubject<string>('');

  public data$ = this.dataSource.asObservable();
  public title$ = this.titleSource.asObservable();


  setTitle(title: string) {
    this.titleSource.next(title);
  }


  getTitle() {
    return this.titleSource.value;
  }


  add(item: ICommon) {
    const list = this.dataSource.value;
    if (list.findIndex(i => (i._section === item._section && i.name === item.name)) === -1) {
      list.push(item);
      this.dataSource.next(list);
    }
  }


  remove(item: ICommon) {
    const list = this.dataSource.value;
    this.dataSource.next(list.filter(i => !(i._section === item._section && i.name === item.name)));
  }


  exists(item: ICommon) {
    return this.dataSource.value.findIndex(i => (i._section === item._section && i.name === item.name)) > -1;
  }


  clearAll() {
    this.titleSource.next('');
    this.dataSource.next([]);
  }


  getStoryItems() {
    const list = this.dataSource.value;
    const out = {
      'IFilms': 0,
      'IPeople': 0,
      'IPlanets': 0,
      'ISpecies': 0,
      'IStarShips': 0,
      'IVehicles': 0,
    };
    for (let i = 0; i < list.length; i++) {
      out[list[i]._section]++;
    }
    return out;
  }


  plotFirst(section, defaultText?, quotes?) {
    const list = this.dataSource.value.filter(i => i._section === section);
    const q = quotes || '';
    const d = defaultText || '';

    if (list.length > 0 && list[0]._section === section) {
      return q + list[0].name + q;
    }
    return d !== '' ? q + d + q : '';
  }


  plotAll(section, defaultText?, quotes?) {
    const list = this.dataSource.value.filter(i => i._section === section).map(i => i.name);
    const q = quotes || '';
    const d = defaultText || '';

    let out = '';
    if (list.length > 0) {
      for (let i = 0; i < list.length - 1; i++) {
        out += q + list[i] + q;
        out += i < list.length - 2 ? ', ' : ' and ';
      }
      out += q + list[list.length - 1] + q;
    } else {
      out = d !== '' ? q + d + q : '';
    }

    return out;
  }


  plotSkipFirst(section, defaultText?, quotes?) {
    const list = this.dataSource.value.filter(i => i._section === section).map(i => i.name);
    const q = quotes || '';
    const d = defaultText || '';

    let out = '';
    if (list.length > 1) {
      for (let i = 1; i < list.length - 1; i++) {
        out += q + list[i] + q;
        out += i < list.length - 2 ? ', ' : ' and ';
      }
      out += q + list[list.length - 1] + q;
    } else {
      out = d !== '' ? q + d + q : '';
    }

    return out;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class ICanHazDadJokesService {

  constructor(public http: HttpClient) {
  }


  getOne() {
    return this.http.get<IICanHazDadJokes>('https://icanhazdadjoke.com/slack');
  }
}

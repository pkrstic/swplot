import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class ChuckNorrisService {

  constructor(public http: HttpClient) {
  }


  getOne() {
    return this.http.get<IChuckNorris>('https://api.chucknorris.io/jokes/random');
  }
}

/**
 *
 * Plot generator, it takes elements from PlotDataService, generates story
 * First it loads two jokes from Chuck Norris API and ICanHasDadJokes API
 * After that starts generating story, for missing resources it is using
 * some fallback content, story line is always the same
 */
import { Component, OnInit } from '@angular/core';
import { PlotDataService } from '../services/plot-data.service';
import { ChuckNorrisService } from '../services/chuck-norris.service';
import { ICanHazDadJokesService } from '../services/ican-haz-dad-jokes.service';


@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.sass'],
})
export class StoryComponent implements OnInit {

  plot: string;
  title: string;
  jokeCN: string;
  jokeICHDJ: string;


  constructor(
    public plotData: PlotDataService,
    public CNJoke: ChuckNorrisService,
    public ICHDJ: ICanHazDadJokesService,
  ) {
  }


  ngOnInit() {
  }


  generateStory() {
    this.CNJoke.getOne().subscribe(
      (result) => {
        this.jokeCN = result.value;
      },
      () => {
        this.jokeCN = 'Ha, just kidding, there is no joke, let\'s eat something!';
      },
      () => {
        this.loadOtherJoke();
      });
  }


  loadOtherJoke() {
    this.ICHDJ.getOne().subscribe(
      (result) => {
        this.jokeICHDJ = result.attachments[0].text;
      },
      () => {
        this.jokeICHDJ = 'I agree!';
      },
      () => {
        this.doPlot();
      });
  }


  doPlot() {
    this.title = this.plotData.getTitle();

    const elements = this.plotData.getStoryItems();

    const supportNumber = Math.floor(Math.random() * 5);
    const support = {
      location: () => {
        const list = ['restaurant', 'coffee shop', 'vehicle service', 'bakery', 'grocery store'];
        return list[supportNumber];
      },
      joker: () => {
        const list = ['waiter', 'barista', 'mechanic', 'clerk', 'cashier'];
        return list[supportNumber];
      },
      response: () => {
        const list = ['great', 'hilarious', 'awesome', 'boring', 'lousy', 'bad', 'terrible'];
        return list[Math.floor(Math.random() * list.length)];
      },
    };

    // ------------------------------

    let lead = 'Young adventurer';
    if (elements['IPeople'] > 0) {
      lead = this.plotData.plotFirst('IPeople');
    }

    // ------------------------------

    this.plot = '';
    this.plot += 'Brave ';

    if (elements['IPeople'] === 0) {
      this.plot += 'young adventurer';
    } else if (elements['IPeople'] === 1) {
      this.plot += 'adventurer ' + lead;
    } else {
      this.plot += 'group of adventurers ' + this.plotData.plotAll('IPeople');
    }

    this.plot += ' enters into ' + support.location() + ' which was part of ';
    this.plot += this.plotData.plotFirst('IFilms', '', '"') + ' shopping center. ';

    this.plot += 'From there they could see other shops';
    this.plot += elements['IFilms'] > 1 ? ', like ' + this.plotData.plotSkipFirst('IFilms', '', '"') : '';
    this.plot += '. ';
    this.plot += '<br>';

    this.plot += lead + ' asks ';

    if (elements['IPeople'] === 0) {
      this.plot += support.joker();
    } else if (elements['IPeople'] === 1) {
      this.plot += this.plotData.plotSkipFirst('IPeople');
    } else {
      this.plot += 'others';
    }
    this.plot += ': ';
    this.plot += '<br>';

    this.plot += '"I\'ve heard this joke couple days ago, and I want to share it with you."';
    this.plot += '<br>';

    this.plot += '"' + this.jokeCN + '"';
    this.plot += '<br>';

    this.plot += '- "Joke is ' + support.response() + '" - replied ';
    if (elements['IPeople'] === 0) {
      this.plot += support.joker();
    } else if (elements['IPeople'] === 1) {
      this.plot += this.plotData.plotSkipFirst('IPeople');
    } else {
      this.plot += 'everyone';
    }
    this.plot += ', "here is better one"';
    this.plot += '<br>';

    this.plot += '"' + this.jokeICHDJ + '"';
    this.plot += '<br>';

    this.plot += 'There was much more jokes to tell, ';
    if (elements['IPeople'] === 1) {
      this.plot += ' and ' + lead + ' asks ' + support.joker() + ' to joins him in new adventure, and after agreeing, they went to';
    } else {
      this.plot += ' but they had to go and';
    }
    this.plot += ' buy new space ship. ';
    this.plot += elements['IVehicles'] > 0 ? 'Seat into ' + this.plotData.plotFirst('IVehicles') : 'Called taxi';
    this.plot += ' and after short trip they arrived at "Space and Furious" vehicle dealer. ';
    this.plot += '<br>';

    this.plot += 'In front of store a lot of space ships and vehicles was parked. They were not interested in vehicles';
    this.plot += elements['IVehicles'] > 1 ? ', but saw brand new ' + this.plotData.plotSkipFirst('IVehicles', '', '"') + '. ' : '. ';
    this.plot += 'On the other side of plot, various new space ships were displayed';
    this.plot += elements['IStarShips'] > 1 ? ', like ' + this.plotData.plotAll('IStarShips', '', '"') + '.' : '';
    this.plot += '<br>';

    this.plot += 'After looking around, they ';

    if (elements['ISpaceShips'] > 0) {
      this.plot += 'jumped into ' + this.plotData.plotFirst('ISpaceShips', '', '"') + '.';
    } else {
      this.plot += 'stole closest space ship';
    }
    this.plot += ' and left this ';
    this.plot += elements['IPlanets'] > 0 ? this.plotData.plotFirst('IPlanets') : 'hostile planet';
    this.plot += '. While they were taking off, question "where to go next" was first thing that came out, on some unknown new world ';
    this.plot += elements['IPlanets'] > 1 ? 'or ' + this.plotData.plotSkipFirst('IPlanets') : '';

    this.plot += ' and try to find ' + this.plotData.plotSkipFirst('ISpecies', 'new friendly species') + '.';
    this.plot += '<br>';

    this.plot += 'To be continued...';
  }

}

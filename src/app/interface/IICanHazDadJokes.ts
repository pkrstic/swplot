interface IICanHazDadJokesAttachments {
  'fallback': string;
  'footer': string;
  'text': string;
}


interface IICanHazDadJokes {
  'attachments': IICanHazDadJokesAttachments[];
  'response_type': string;
  'username': string;
}

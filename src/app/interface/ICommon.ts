interface ICommon {
  'name': string;
  'created': string;
  'edited': string;
  'url': string;
  '_section': string;
  '_checked': string;
}

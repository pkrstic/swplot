interface IList {
  'count': number;
  'next': string;
  'previous': string;
  'results': any[];
}

/**
 *
 * Displaying list of selected plot elements
 *
 */
import { Component, OnInit } from '@angular/core';
import { PlotDataService } from '../services/plot-data.service';
import { FilmsService } from '../services/films.service';
import { PeopleService } from '../services/people.service';
import { PlanetsService } from '../services/planets.service';
import { SpeciesService } from '../services/species.service';
import { StarshipsService } from '../services/starships.service';
import { VehiclesService } from '../services/vehicles.service';


@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.sass'],
})
export class SelectionComponent implements OnInit {

  plotTitle = '';


  constructor(
    private plotData: PlotDataService,
    private films: FilmsService,
    private people: PeopleService,
    private planets: PlanetsService,
    private species: SpeciesService,
    private starShips: StarshipsService,
    private vehicles: VehiclesService,
  ) {
  }


  ngOnInit() {
  }


  updateTitle() {
    this.plotData.setTitle(this.plotTitle);
  }


  clearList() {
    this.plotData.clearAll();
    this.films.clearAll();
    this.people.clearAll();
    this.planets.clearAll();
    this.species.clearAll();
    this.starShips.clearAll();
    this.vehicles.clearAll();
  }


  randomElements() {
    this.plotData.clearAll();

    for (let i = 0; i < Math.floor(Math.random() * 4); i++) {
      this.films.getByID('IFilms', Math.floor(Math.random() * this.films.getCount())).subscribe((item) => {
        this.films.checkItem(item);
        this.plotData.add(item);
      });
    }

    for (let i = 0; i < Math.floor(Math.random() * 4); i++) {
      this.people.getByID('IPeople', Math.floor(Math.random() * this.people.getCount())).subscribe((item) => {
        this.people.checkItem(item);
        this.plotData.add(item);
      });
    }

    for (let i = 0; i < Math.floor(Math.random() * 4); i++) {
      this.planets.getByID('IPlanets', Math.floor(Math.random() * this.planets.getCount())).subscribe((item) => {
        this.planets.checkItem(item);
        this.plotData.add(item);
      });
    }

    for (let i = 0; i < Math.floor(Math.random() * 4); i++) {
      this.species.getByID('ISpecies', Math.floor(Math.random() * this.species.getCount())).subscribe((item) => {
        this.species.checkItem(item);
        this.plotData.add(item);
      });
    }

    for (let i = 0; i < Math.floor(Math.random() * 4); i++) {
      this.starShips.getByID('IStarShips', Math.floor(Math.random() * this.starShips.getCount())).subscribe((item) => {
        this.starShips.checkItem(item);
        this.plotData.add(item);
      });
    }

    for (let i = 0; i < Math.floor(Math.random() * 4); i++) {
      this.vehicles.getByID('IVehicles', Math.floor(Math.random() * this.vehicles.getCount())).subscribe((item) => {
        this.vehicles.checkItem(item);
        this.plotData.add(item);
      });
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { FilmsService } from '../../services/films.service';
import { FilterComponent } from '../filters.component';
import { PlotDataService } from '../../services/plot-data.service';


@Component({
  selector: 'app-films',
  templateUrl: '../filters.component.html',
  styleUrls: ['../filters.component.sass'],
})
export class FilmsComponent extends FilterComponent implements OnInit {

  constructor(
    public dataService: FilmsService,
    public plotData: PlotDataService,
  ) {
    super(dataService, plotData);
    this.title = 'Films';
    this.itemType = 'IFilms';
  }
}

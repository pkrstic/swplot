/**
 *
 * Common component for manipulating resources
 * Loading data, displaying and selecting plot elements
 *
 * Every resource type has its own controller and service
 * @see app/core/BaseService for resources service common class
 *
 */
import { OnInit } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { CommonService } from '../core/config';
import { PlotDataService } from '../services/plot-data.service';


export class FilterComponent implements OnInit {

  title = '';
  list: any[];
  searchTerm = '';
  loading = false;
  errorLoading = false;
  loadPrev = '';
  loadNext = '';
  itemType = '';


  constructor(
    public dataService: CommonService,
    public plotData: PlotDataService,
  ) {
  }


  ngOnInit() {
    this.getData();
  }


  /**
   * Select / deselect item on list
   */
  onItemChange(item) {
    if (!item._checked) {
      this.dataService.checkItem(item);
      this.plotData.add(item);
    } else {
      this.dataService.uncheckItem(item);
      this.plotData.remove(item);
    }
  }


  handleSearch() {
    const params = new HttpParams().set('search', this.searchTerm);

    this.loading = true;
    this.errorLoading = false;

    this.dataService.search(this.itemType, params).subscribe(
      (result) => {
        this.list = result.results;
      },
      (err) => {
        console.log(err);
        this.errorLoading = true;
      },
      () => {
        this.loading = false;
      },
    );
  }


  handleLoadPrev() {
    this.getData(this.loadPrev);
  }


  handleLoadNext() {
    this.getData(this.loadNext);
  }


  getData(url?: string) {
    this.loading = true;
    this.errorLoading = false;

    this.dataService.getAll(this.itemType, url).subscribe(
      (result) => {
        this.list = result.results;
        this.loadPrev = result.previous;
        this.loadNext = result.next;
      },
      (err) => {
        console.log(err);
        this.errorLoading = true;
      },
      () => {
        this.loading = false;
      },
    );
  }
}

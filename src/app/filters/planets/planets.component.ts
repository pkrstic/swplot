import { Component, OnInit } from '@angular/core';
import { FilterComponent } from '../filters.component';
import { PlanetsService } from '../../services/planets.service';
import { PlotDataService } from '../../services/plot-data.service';


@Component({
  selector: 'app-planets',
  templateUrl: '../filters.component.html',
  styleUrls: ['../filters.component.sass'],
})
export class PlanetsComponent extends FilterComponent implements OnInit {

  constructor(
    public dataService: PlanetsService,
    public plotData: PlotDataService,
  ) {
    super(dataService, plotData);
    this.title = 'Planets';
    this.itemType = 'IPlanets';
  }
}

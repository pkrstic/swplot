import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../../services/people.service';
import { FilterComponent } from '../filters.component';
import { PlotDataService } from '../../services/plot-data.service';


@Component({
  selector: 'app-people',
  templateUrl: '../filters.component.html',
  styleUrls: ['../filters.component.sass'],
})
export class PeopleComponent extends FilterComponent implements OnInit {

  constructor(
    public dataService: PeopleService,
    public plotData: PlotDataService,
  ) {
    super(dataService, plotData);
    this.title = 'People';
    this.itemType = 'IPeople';
  }

}

import { Component, OnInit } from '@angular/core';
import { StarshipsService } from '../../services/starships.service';
import { FilterComponent } from '../filters.component';
import { PlotDataService } from '../../services/plot-data.service';


@Component({
  selector: 'app-starships',
  templateUrl: '../filters.component.html',
  styleUrls: ['../filters.component.sass'],
})
export class StarshipsComponent extends FilterComponent implements OnInit {

  constructor(
    public dataService: StarshipsService,
    public plotData: PlotDataService,
  ) {
    super(dataService, plotData);
    this.title = 'Star ships';
    this.itemType = 'IStarShips';
  }
}

import { Component, OnInit } from '@angular/core';
import { FilterComponent } from '../filters.component';
import { SpeciesService } from '../../services/species.service';
import { PlotDataService } from '../../services/plot-data.service';


@Component({
  selector: 'app-species',
  templateUrl: '../filters.component.html',
  styleUrls: ['../filters.component.sass'],
})
export class SpeciesComponent extends FilterComponent implements OnInit {

  constructor(
    public dataService: SpeciesService,
    public plotData: PlotDataService,
  ) {
    super(dataService, plotData);
    this.title = 'Species';
    this.itemType = 'ISpecies';
  }
}

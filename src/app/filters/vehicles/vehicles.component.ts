import { Component, OnInit } from '@angular/core';
import { VehiclesService } from '../../services/vehicles.service';
import { FilterComponent } from '../filters.component';
import { PlotDataService } from '../../services/plot-data.service';


@Component({
  selector: 'app-vehicles',
  templateUrl: '../filters.component.html',
  styleUrls: ['../filters.component.sass'],
})
export class VehiclesComponent extends FilterComponent implements OnInit {

  constructor(
    public dataService: VehiclesService,
    public plotData: PlotDataService,
  ) {
    super(dataService, plotData);
    this.title = 'Vehicles';
    this.itemType = 'IVehicles';
  }
}
